module.exports = {
	env: {
		browser: true,
		commonjs: true,
		es2021: true,
	},
	extends: ['airbnb-base'],
	parserOptions: {
		ecmaVersion: 12,
	},
	rules: {
		indent: [2, 'tab', { SwitchCase: 1 }],
		'linebreak-style': [0, 'false'],
		allowIndentationTabs: [0, 'true'],
		'no-tabs': 0,
	},
};
