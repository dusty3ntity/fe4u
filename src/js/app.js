/* eslint-disable comma-dangle */
/* eslint-disable no-console */
/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
/* eslint-disable no-use-before-define */
/* eslint-disable object-curly-newline */

import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import L from 'leaflet';
import Chart from 'chart.js/auto';
import _ from 'lodash';

import { normalizeUser, validateUser, searchUsers, generateColor } from './util';
import { allCountries } from './constants';

import '../scss/style.scss';

const BASE_URL = 'https://randomuser.me/api/';
const BASE_USERS_COUNT = 50;

let allTeachers = [];
let filteredTeachers = allTeachers;
let searchQuery = '';
let filters = {};
let favoriteTeachersCarouselPage = 0;

const fetchTeachers = async (count, append) => {
	const url = new URL(BASE_URL);

	if (count) {
		url.searchParams.append('results', count);
	} else {
		url.searchParams.append('results', BASE_USERS_COUNT);
	}

	if (filters) {
		_.forEach(Object.entries(filters), ([key, value]) => url.searchParams.append(key, value.toLowerCase()));
	}

	const response = await fetch(url.toString());
	if (!response.ok) {
		throw new Error('Error getting users');
	}

	const { results } = await response.json();
	if (append) {
		allTeachers = [...allTeachers, ..._.map(results, normalizeUser)];
	} else {
		allTeachers = _.map(results, normalizeUser);
	}
};

const filterTeachers = () => {
	if (searchQuery.length === 0) {
		filteredTeachers = allTeachers;
	} else {
		filteredTeachers = searchUsers(allTeachers, searchQuery);
	}
};

/* --- new teacher dialog --- */

const closeNewTeacherDialog = () => {
	const dialog = document.getElementById('add-teacher-dialog');
	const blanket = document.querySelector('.blanket');
	dialog.remove();
	blanket.remove();
	document.getElementById('root').classList.toggle('blurred');
};

const onNewTeacherDialogSubmit = async () => {
	const dialog = document.getElementById('add-teacher-dialog');

	const result = {
		full_name: dialog.querySelector('#name').value,
		country: dialog.querySelector('#country').value,
		city: dialog.querySelector('#city').value,
		phone: dialog.querySelector('#phone').value,
		email: dialog.querySelector('#email').value,
		age: Number.parseInt(dialog.querySelector('#age').value, 10),
		gender: dialog.querySelector('input[name="gender"]:checked').value,
		bg_color: dialog.querySelector('#bg-color').value,
	};

	const validationErrors = validateUser(result);
	if (!validationErrors.length) {
		const url = 'http://localhost:3000';
		const requestParams = {
			headers: {
				'content-type': 'application/json',
			},
			body: JSON.stringify(result),
			method: 'POST',
		};
		await fetch(url, requestParams);
		closeNewTeacherDialog();
		return;
	}

	const validationMessages = _.map(validationErrors, (e) => e.message);
	const errorsList = dialog.querySelector('.errors-list');
	errorsList.innerHTML = '';
	_.forEach(validationMessages, (m) => {
		const error = document.createElement('li');
		error.innerHTML = m;
		errorsList.appendChild(error);
	});
};

const openNewTeacherDialog = () => {
	const template = document.getElementById('add-teacher-dialog-template');

	const dialog = template.content.cloneNode(true);
	const countrySelector = dialog.querySelector('#country');
	_.forEach(allCountries, (c) => {
		const option = document.createElement('option');
		option.text = c;
		countrySelector.appendChild(option);
	});
	dialog.querySelector('.close-btn').addEventListener('click', closeNewTeacherDialog);
	dialog.querySelector('#add-teacher-form').addEventListener('submit', async (e) => {
		e.preventDefault();
		await onNewTeacherDialogSubmit();
	});

	document.getElementById('root').classList.toggle('blurred');
	const blanket = document.createElement('div');
	blanket.classList.add('blanket');
	document.body.appendChild(blanket);
	document.body.appendChild(dialog);
};

const configureNewTeacherDialog = () => {
	const addTeacherBtns = document.querySelectorAll('.add-teacher-btn');
	_.forEach(addTeacherBtns, (b) => b.addEventListener('click', openNewTeacherDialog));
};

/* --- teacher card dialog --- */

const closeTeacherDialog = () => {
	const dialog = document.getElementById('teacher-card-dialog');
	const blanket = document.querySelector('.blanket');
	dialog.remove();
	blanket.remove();
	document.getElementById('root').classList.toggle('blurred');
};

const handleStarClick = (teacher) => {
	teacher.favorite = !teacher.favorite;
	renderTopTeachersList();
	renderFavoriteTeachersList();
};

const openTeacherDialog = (teacher) => {
	const template = document.getElementById('teacher-card-dialog-template');

	const dialog = template.content.cloneNode(true);
	dialog.querySelector('.avatar').setAttribute('src', teacher.picture_large);

	dialog.querySelector('.name').innerHTML = teacher.full_name;
	dialog.querySelector('.city').innerHTML = teacher.city;
	dialog.querySelector('.country').innerHTML = teacher.country;

	dialog.querySelector('.age').innerHTML = teacher.age;
	dialog.querySelector('.gender').innerHTML = teacher.gender.charAt(0);
	dialog.querySelector('.gender').setAttribute('aria-label', teacher.gender);

	dayjs.extend(relativeTime);
	const dob = dayjs(teacher.b_date);
	const closestBirthday = dob.add(teacher.age + 1, 'year');
	dialog.querySelector('.days-to-birthday').innerHTML = dayjs().to(closestBirthday);

	dialog.querySelector('.email').innerHTML = teacher.email;
	dialog.querySelector('.email').setAttribute('href', `mailto:${teacher.email}`);

	dialog.querySelector('.phone').innerHTML = teacher.phone;
	dialog.querySelector('.note').innerHTML = teacher.note;

	const defaultCoordinates = [50.51676987116671, 30.784363188680512];

	const mapContainer = dialog.querySelector('.map-container');
	const map = L.map(mapContainer).setView(
		defaultCoordinates ?? [teacher.location.coordinates.latitude, teacher.location.coordinates.longitude],
		15
	);
	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
		attribution:
			'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		maxZoom: 18,
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1,
		accessToken: 'pk.eyJ1IjoibmllemVsZDAxIiwiYSI6ImNrd2NveDRlMTE0dHIyc3FscDlhcWVjdHUifQ.Y8I9iEF56WfA4dcncnk82Q',
	}).addTo(map);

	dialog.querySelector('.toggle-map').addEventListener('click', () => {
		if (mapContainer.hasAttribute('hidden')) {
			mapContainer.removeAttribute('hidden');
		} else {
			mapContainer.setAttribute('hidden', true);
		}
	});

	dialog.querySelector('.close-btn').addEventListener('click', closeTeacherDialog);

	const starBtn = dialog.querySelector('.star-btn');
	if (teacher.favorite) {
		starBtn.classList.add('active');
	}
	starBtn.addEventListener('click', () => {
		handleStarClick(teacher);
		starBtn.classList.toggle('active');
	});

	document.getElementById('root').classList.toggle('blurred');
	const blanket = document.createElement('div');
	blanket.classList.add('blanket');
	document.body.appendChild(blanket);
	document.body.appendChild(dialog);
};

/* --- teacher card --- */

const createTeacherCard = (teacher, addStar) => {
	const template = document.getElementById('teacher-card-small-template');

	const teacherCard = template.content.cloneNode(true);
	teacherCard.querySelector('.avatar').setAttribute('src', teacher.picture_thumbnail);
	teacherCard.querySelector('.teacher-name').innerHTML = teacher.full_name.replace(' ', '<br>');
	teacherCard.querySelector('.teacher-country').innerHTML = teacher.country;

	if (addStar && teacher.favorite) {
		teacherCard.childNodes[0].classList.add('starred');
	}

	teacherCard.childNodes[0].addEventListener('click', () => openTeacherDialog(teacher));
	return teacherCard;
};

/* --- top teachers list --- */

const renderTopTeacher = (teacher) => {
	const topTeachersList = document.getElementById('top-teachers-list');
	const teacherCard = createTeacherCard(teacher, true);
	const listItem = document.createElement('li');
	listItem.appendChild(teacherCard);
	topTeachersList.appendChild(listItem);
};

const renderTopTeachersList = () => {
	document.getElementById('top-teachers-list').innerHTML = '';
	filteredTeachers.forEach(renderTopTeacher);
};

const configureLoadMoreButton = () => {
	const btn = document.querySelector('.load-more-teachers-btn');
	btn.addEventListener('click', async () => {
		await fetchTeachers(10, true);
		filterTeachers();
		renderTopTeachersList();
		renderFavoriteTeachersList();
	});
};

/* --- favorite teachers carousel --- */

const renderFavoriteTeacher = (teacher) => {
	const carousel = document.getElementById('favorite-teachers-list');
	const teacherCard = createTeacherCard(teacher, true);
	const listItem = document.createElement('li');
	listItem.appendChild(teacherCard);
	carousel.appendChild(listItem);
};

const getRootScale = () => parseFloat(getComputedStyle(document.querySelector('body'))['font-size']);

const configureFavoriteTeachersCarousel = () => {
	const OFFSET = 17.6;
	const carousel = document.querySelector('#favorites .carousel');
	const list = carousel.querySelector('#favorite-teachers-list');
	const prevBtn = carousel.querySelector('.prev-btn');
	const nextBtn = carousel.querySelector('.next-btn');

	prevBtn.addEventListener('click', () => {
		const carouselHeight = list.scrollHeight / getRootScale();
		if (favoriteTeachersCarouselPage === 0 || carouselHeight / OFFSET < 1.5) {
			return;
		}

		favoriteTeachersCarouselPage -= 1;
		list.setAttribute('style', `transform: translate(0, ${OFFSET * favoriteTeachersCarouselPage}rem);`);
	});
	nextBtn.addEventListener('click', () => {
		const carouselHeight = list.scrollHeight / getRootScale();
		if (Math.abs(carouselHeight - favoriteTeachersCarouselPage * OFFSET) < OFFSET || carouselHeight / OFFSET < 1.5) {
			return;
		}

		favoriteTeachersCarouselPage += 1;
		list.setAttribute('style', `transform: translate(0, -${OFFSET * favoriteTeachersCarouselPage}rem);`);
	});
};

const renderFavoriteTeachersList = () => {
	const list = document.getElementById('favorite-teachers-list');
	list.innerHTML = '';
	list.style = '';

	filteredTeachers.filter((t) => t.favorite).forEach(renderFavoriteTeacher);
};

/* --- search --- */

const configureSearch = () => {
	document.getElementById('search-btn').addEventListener('click', () => {
		searchQuery = document.getElementById('search-input').value;
		filterTeachers();
		renderTopTeachersList();
		renderFavoriteTeachersList();
	});
};

/* --- filters --- */

const collectFiltersData = () => {
	let result = {};
	const isCountryFilterEnabled = document.getElementById('filter-by-country').checked;
	if (isCountryFilterEnabled) {
		const country = document.getElementById('filter-country').value;
		result = { ...result, country };
	}

	const isAgeFilterEnabled = document.getElementById('filter-by-age').checked;
	if (isAgeFilterEnabled) {
		const age = document.getElementById('filter-age').value;
		if (!Number.isNaN(age)) {
			result = { ...result, age };
		}
	}

	const isGenderFilterEnabled = document.getElementById('filter-by-gender').checked;
	if (isGenderFilterEnabled) {
		const gender = document.getElementById('filter-gender').value;
		result = { ...result, gender };
	}

	return result;
};

const configureFilters = () => {
	const countrySelector = document.getElementById('filter-country');
	allCountries.forEach((c) => {
		const option = document.createElement('option');
		option.text = c;
		countrySelector.appendChild(option);
	});

	document.getElementById('apply-filters').addEventListener('click', async () => {
		filters = collectFiltersData();
		await fetchTeachers();
		filterTeachers();
		renderTopTeachersList();
		renderFavoriteTeachersList();
	});
};

/* --- charts --- */

const gendersChart = new Chart(document.getElementById('genders-chart'), {
	type: 'pie',
	data: {
		labels: ['Female', 'Male'],
		datasets: [
			{
				backgroundColor: ['rgba(255,99,132,0.5)', 'rgba(54, 162, 235, 0.5)'],
				borderColor: ['rgba(255, 99, 132, 1)', 'rgba(54, 162, 235, 1)'],
				borderWidth: 1,
			},
		],
	},
	options: {
		plugins: {
			title: {
				display: true,
				text: 'Teachers by gender',
			},
		},
	},
});

const countriesChart = new Chart(document.getElementById('countries-chart'), {
	type: 'bar',
	data: {
		datasets: [
			{
				label: 'Teachers',
				backgroundColor: [],
				borderColor: [],
				borderWidth: 1,
			},
		],
	},
	options: {
		plugins: {
			title: {
				display: true,
				text: 'Teachers by country',
			},
		},
		scales: {
			y: {
				beginAtZero: true,
			},
		},
	},
});

const agesChart = new Chart(document.getElementById('ages-chart'), {
	type: 'bar',
	data: {
		datasets: [
			{
				label: 'Teachers',
				backgroundColor: [],
				borderColor: [],
				borderWidth: 1,
			},
		],
	},
	options: {
		plugins: {
			title: {
				display: true,
				text: 'Teachers by age',
			},
		},
	},
});

const configureGendersChart = (teachers) => {
	const femalesCount = _.filter(teachers, { gender: 'female' }).length;
	const malesCount = teachers.length - femalesCount;
	gendersChart.data.datasets[0].data = [femalesCount, malesCount];
	gendersChart.update();
};

const configureCountriesChart = (teachers) => {
	const countries = new Set(_.map(teachers, (t) => t.country));
	const labels = [];
	const values = [];
	countries.forEach((c) => {
		values.push(_.filter(teachers, { country: c }).length);
		labels.push(c);
	});
	const backgroundColors = _.map(labels, generateColor);
	countriesChart.data.labels = labels;
	countriesChart.data.datasets[0].data = values;
	countriesChart.data.datasets[0].backgroundColor = backgroundColors;
	console.log(countriesChart);
	countriesChart.update();
};

const configureAgesChart = (teachers) => {
	const labels = ['18-30', '31-40', '41-50', '51-60', '61-70', ' 71-80', '81-90', '91-100'];
	const values = [0, 0, 0, 0, 0, 0, 0, 0];
	_.forEach(teachers, (t) => {
		const { age } = t;
		if (age < 31) {
			values[0] += 1;
		} else if (age < 41) {
			values[1] += 1;
		} else if (age < 51) {
			values[2] += 1;
		} else if (age < 61) {
			values[3] += 1;
		} else if (age < 71) {
			values[4] += 1;
		} else if (age < 81) {
			values[5] += 1;
		} else if (age < 91) {
			values[6] += 1;
		} else {
			values[7] += 1;
		}
	});
	agesChart.data.labels = labels;
	agesChart.data.datasets[0].data = values;
	agesChart.data.datasets[0].backgroundColor = _.map(labels, generateColor);
	agesChart.update();
};

const configureCharts = (teachers) => {
	configureGendersChart(teachers);
	configureCountriesChart(teachers);
	configureAgesChart(teachers);
};

/* --- initialization --- */

const init = async () => {
	try {
		await fetchTeachers();
		filteredTeachers = allTeachers;
		configureLoadMoreButton();
		renderTopTeachersList();
		configureFavoriteTeachersCarousel();
		renderFavoriteTeachersList();
		configureSearch();
		configureFilters();
		configureNewTeacherDialog();
		configureCharts(filteredTeachers);
	} catch (e) {
		console.error(e);
	}
};

init();
