/* eslint-disable implicit-arrow-linebreak */
/* eslint-disable max-len */
/* eslint-disable prefer-destructuring */
/* eslint-disable camelcase */

import _ from 'lodash';

const EMAIL_REGEX = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const PHONE_REGEX = /^\+\d{1,2}\(\d{3}\)\d\d-\d\d-\d{3}$/; // format: +38(000)00-00-000 or +1(000)00-00-000

export const generateId = (length) => {
	let result = '';
	const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	for (let i = 0; i < length; i += 1) {
		result += characters.charAt(Math.floor(Math.random() * characters.length));
	}
	return result;
};

// eslint-disable-next-line no-bitwise
export const generateColor = () => `#${(((1 << 24) * Math.random()) | 0).toString(16)}`;

export const generateFavourite = () => Math.random() < 0.05;

export const generateCourse = () => {
	const courses = ['Math', 'English', 'Chemistry', 'Science', 'Music', 'French', 'History'];
	const index = Math.floor(Math.random() * courses.length);
	return courses[index];
};

export const calculateAge = (dateOfBirth) => {
	const monthDiff = Date.now() - dateOfBirth.getTime();
	const ageDate = new Date(monthDiff);
	const year = ageDate.getUTCFullYear();
	const age = Math.abs(year - 1970);
	return age;
};

export const compareStrings = (a, b, order) => {
	const stringA = a.toUpperCase();
	const stringB = b.toUpperCase();
	if (stringA < stringB) {
		return order === 'desc' ? 1 : -1;
	}
	if (stringA > stringB) {
		return order === 'desc' ? -1 : 1;
	}
	return 0;
};

export const isNonEmptyString = (val) => typeof val === 'string' && val.length > 0;

export const isTitleCaseString = (val) => {
	if (!isNonEmptyString(val)) {
		return false;
	}
	return val.charAt(0) === val.charAt(0).toUpperCase();
};

export const normalizeUser = (user) => {
	const id = user.login?.uuid ?? user.id;
	const favorite = user.favorite;
	const course = user.course;
	const bg_color = user.bg_color;
	const note = user.note;

	const gender = user.gender;
	const title = user.name?.title ?? user.title;
	const full_name = user.name ? `${user.name.first} ${user.name.last}` : user.full_name;
	const city = user.location?.city ?? user.city;
	const state = user.location?.state ?? user.state;
	const country = user.location?.country ?? user.country;
	const location = user.location;
	const postcode = user.location?.postcode ?? user.postcode;
	const coordinates = user.location?.coordinates ?? user.coordinates;
	const timezone = user.location?.timezone ?? user.timezone;
	const email = user.email;
	const b_date = user.dob?.date ?? user.b_day;
	let age = user.dob?.age;
	if (!age && b_date) {
		age = calculateAge(new Date(b_date));
	}
	const phone = user.phone;
	const picture_large = user.picture?.large ?? user.picture_large;
	const picture_thumbnail = user.picture?.thumbnail ?? user.picture_thumbnail;

	return {
		id: id ?? generateId(10),
		favorite: favorite ?? generateFavourite(),
		course: course ?? generateCourse(),
		bg_color: bg_color ?? generateColor(),
		note: note ?? '',
		gender: gender ?? 'Not specified',
		title: title ?? '',
		full_name: full_name ?? 'Anonymous',
		city: city ?? 'Kyiv',
		state: state ?? '',
		country: country ?? 'Ukraine',
		location,
		postcode: postcode ?? 7200,
		coordinates: coordinates ?? null,
		timezone: timezone ?? null,
		email: email ?? 'anonymous@test.com',
		b_date: b_date ?? '01.01.1970',
		age: age ?? '51',
		phone: phone ?? '+38(000)00-00-000',
		picture_large: picture_large ?? 'https://i.imgur.com/MicKmag.png',
		picture_thumbnail: picture_thumbnail ?? 'https://i.imgur.com/MicKmag.png',
	};
};

export const validateUser = (user) => {
	const validationSchema = {
		full_name: isTitleCaseString,
		gender: isTitleCaseString,
		city: isTitleCaseString,
		country: isTitleCaseString,
		age: (val) => typeof val === 'number' && Number.isInteger(val),
		phone: (val) => isNonEmptyString(val) && PHONE_REGEX.test(val),
		email: (val) => isNonEmptyString(val) && EMAIL_REGEX.test(val),
	};

	const errors = Object.keys(validationSchema)
		.filter((constraintKey) => !validationSchema[constraintKey](user[constraintKey]))
		.map((constraintKey) => new Error(`${constraintKey} is invalid`));
	return errors;
};

export const filterUsers = (users, request) => {
	const schema = {
		picture_large: (val) => !!val,
		favorite: (val) => !!val,
		country: (val) => val.toLowerCase() === request.country.toLowerCase(),
		age: (val) => val > request.age,
		gender: (val) => val.toLowerCase() === request.gender.toLowerCase(),
	};

	const constraints = {};
	Object.keys(schema)
		.filter((constraintKey) => !!request[constraintKey])
		.forEach((constraintKey) => {
			constraints[constraintKey] = schema[constraintKey];
		});
	const result = users.filter(
		(user) =>
			// eslint-disable-next-line comma-dangle
			Object.keys(constraints).every((constraintKey) => constraints[constraintKey](user[constraintKey]))
		// eslint-disable-next-line function-paren-newline
	);
	return result;
};

export const sortUsers = (users, sortBy, order) => {
	switch (sortBy) {
		case 'full_name':
			return users.sort((a, b) => compareStrings(a.full_name, b.full_name, order));
		case 'country':
			return users.sort((a, b) => compareStrings(a.country, b.country, order));
		case 'age':
			return users.sort((a, b) => (order === 'asc' ? a.age - b.age : b.age - a.age));
		case 'gender':
			return users.sort((a, b) => compareStrings(a.gender, b.gender, order));
		case 'b_day':
			return users.sort((a, b) => {
				if (order === 'asc') {
					return new Date(a.b_date) - new Date(b.d_date);
				}
				return new Date(b.b_date) - new Date(a.d_date);
			});
		default:
			return users;
	}
};

// I've implemented a search function to perform search by one query which can include name,
// note and age together to match the provided design
export const searchUsers = (users, query) => {
	const userSearchStrings = _.map(users, (u) => `${u.full_name} ${u.note} ${u.age}`.toLowerCase());
	const queryParts = query.toLowerCase().split(' ');

	const resultValues = _.map(userSearchStrings, (uss) => queryParts.every((qp) => uss.includes(qp)));
	return users.filter((__, ind) => resultValues[ind]);
};

export const getSearchPercentage = (users, searchResult) => {
	const allUsersCount = users.length;
	const searchUsersCount = searchResult.length;

	return Math.floor((searchUsersCount / allUsersCount) * 100);
};
